﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Model
{
    public class SchoolClass
    {
        private Random i = new Random();
        private string name;
        private List<Student> students=new List<Student>();
        
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value.Contains("-")) {
                    name = value;
                }
                else
                {
                    throw new Exception();
                }
                
            }
        }
        public List<Student> Students
        {
            get
            {
                return students;
            }
            set
            {
                students=value;
            }
        }

        public Student GetRandomStudent()
        {
            return students[i.Next(0, students.Count-1)];
        }
    }
}
