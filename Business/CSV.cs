﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class CSV
    {
        static public List<string[]> ImportCSV()
        {
            List<string[]> values = new List<string[]>();
            if (File.Exists(@"..\..\csv\SI-C3a.csv"))
            {
                using (StreamReader reader = new StreamReader(@"..\..\csv\SI-C3a.csv"))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        values.Add(line.Split(','));
                        //importedStudentList.Add(new Student(values[0], values[1]));
                    }
                }
                string date = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                File.Copy(@"..\..\csv\SI-C3a.csv", @"..\..\csv\imported_" + date + ".csv");
            }
            return values;
        }
        static public void modifyStudent(Student student, string textBox2, string textBox1)
        {
            using (StreamWriter writer = new StreamWriter(@"..\..\csv\TempSI-C3a.csv"))
            {
                using (StreamReader reader = new StreamReader(@"..\..\csv\SI-C3a.csv"))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        string[] values = line.Split(',');

                        if (values[0].ToLower() == student.Firstname.ToLower() & values[1].ToLower() == student.Lastname.ToLower())
                        {
                            writer.WriteLine(textBox2 + "," + textBox1);
                        }
                        else
                        {
                            writer.WriteLine(line);
                        }
                    } 

                }
                if (student.Firstname == "" & student.Lastname == "")
                {
                    writer.WriteLine(textBox2 + "," + textBox1);
                    student.Firstname = textBox2;
                    student.Lastname = textBox1;
                }
            }
            File.Delete(@"..\..\csv\SI-C3a.csv");
            File.Move(@"..\..\csv\TempSI-C3a.csv", @"..\..\csv\SI-C3a.csv");
        }
    }
}
