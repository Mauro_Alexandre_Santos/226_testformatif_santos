﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Student
    {
        string firstName, lastName;
        public Student()
        {

        }

        public Student(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName.ToUpper();
        }

        public string Firstname
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }
        public string Lastname
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value.ToUpper();
            }
        }
        public override string ToString()
        {
            return Firstname+" "+Lastname;
        }
    }
}
