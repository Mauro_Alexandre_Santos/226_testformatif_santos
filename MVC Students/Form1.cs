﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
namespace MVC_Students
{
    public partial class Class : Form
    {
        SchoolClass theClass = new SchoolClass();
        public Class()
        {
            InitializeComponent();
        }

        private void ListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex !=-1) {
                Form2 m = new Form2(theClass.Students[listBox2.SelectedIndex]);
                DialogResult result = m.ShowDialog();
                if (result == DialogResult.OK)
                {
                    theClass.Students[listBox2.SelectedIndex] = m.student;
                    showStudentsFromClass(theClass);
                }
                /**
                if (result == DialogResult.Cancel)
                {
                    MessageBox.Show("Cancel Button clicked");
                }
                
                if ((result != DialogResult.OK) && ((result != DialogResult.Cancel)))
                {
                    throw new Exception();
                }
                **/
            }
        }

        private void ListBox1_Click(object sender, EventArgs e)
        {
            List<string[]> listStudents = CSV.ImportCSV();
            foreach (string[] studentData in listStudents)
            {
                theClass.Students.Add(new Student(studentData[0], studentData[1]));
            }
            showStudentsFromClass(theClass);
        }

        private void Class_Load(object sender, EventArgs e)
        {

        }
        private void showStudentsFromClass(SchoolClass selectedClass)
        {
            listBox2.Items.Clear();
            foreach (Student i in selectedClass.Students)
            {
                listBox2.Items.Add(i.ToString());
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Form2 m = new Form2();
            DialogResult result = m.ShowDialog();
            if (result == DialogResult.OK)
            {
                theClass.Students.Add(m.student);
                showStudentsFromClass(theClass);
                button1.Visible = true;
            }
        }
        

    }
}
