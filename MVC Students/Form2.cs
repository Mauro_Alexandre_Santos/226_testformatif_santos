﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace MVC_Students
{
    public partial class Form2 : Form
    {
       public Student student = new Student("","");
        public Form2()
        {
            InitializeComponent();
            foreach (Control cont in groupBox2.Controls)
            {
                cont.Enabled = true;
            }
            button1.Visible = true;
            button1.Enabled = true;
            pictureBox1.Image = pictureBox1.ErrorImage;
        }
        public Form2(Student inputStudent)
        {
            InitializeComponent();
            student = inputStudent;
            textBox2.Text = student.Firstname;
            textBox1.Text = student.Lastname;
            groupBox1.Text = student.Firstname + " " + student.Lastname;
            string path = @"..\..\images\\" + student.Firstname + "_" + student.Lastname + ".jpg";
            if (File.Exists(path))
            {
                using (Bitmap bmpTemp = new Bitmap(path))
                {
                    pictureBox1.Image = new Bitmap(bmpTemp);
                }
            }
            else
            {
                pictureBox1.Image = pictureBox1.ErrorImage;
            }
            
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
        private void Button1_Click(object sender, EventArgs e)
        {
        
            string path = @"..\..\images\\" + student.Firstname + "_" + student.Lastname + ".jpg";
            string path2 = @"..\..\images\\" + textBox2.Text + "_" + textBox1.Text + ".jpg";
            if (File.Exists(path))
            {
                System.IO.File.Move(path, path2);
            }
            CSV.modifyStudent(student,textBox2.Text,textBox1.Text);
            student.Firstname = textBox2.Text;
            student.Lastname = textBox1.Text;

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            //this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            foreach (Control cont in groupBox2.Controls)
            {
                cont.Enabled = true;
            }
            button1.Visible = true;

        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            string path = @"..\..\images";
            string newPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), path));
            Process.Start("explorer.exe", newPath);
        }
    }
}
